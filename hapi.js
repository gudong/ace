'use strict';

var Hapi = require('hapi');
// var port = 8001;

var server = new Hapi.Server();
server.connection({ 
	// host: '127.0.0.1', 
	port: 8001 
});



server.register(require('inert'), function (err) {

	if (err) {
		throw err;
	}
	server.route({
		method: 'GET',
		path: '/',
		handler: function(request, reply) {
			reply.file('./index.html');
			// reply('hello')
		}
	});

	server.start(function() {
		console.log('Server running at:', server.info.uri);
	});

});